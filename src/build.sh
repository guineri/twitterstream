mkdir classes

CPFILES=''

JFILES=''
for f in $(find ./java/org/parallelus/samples/twitterstream/ -name "*.java"); do
   JFILES=$JFILES' '$f
done

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/guineri/StudioProjects/TwitterStream/src/libs/

javac -cp .:$CPFILES -d ./classes/ $JFILES
jar cf ./twitterstream.jar -C classes/ .
rm -r classes
