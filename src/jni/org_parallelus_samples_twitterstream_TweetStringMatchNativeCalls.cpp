#include <parallelus/ParallelUS.hpp>
#include <stddef.h>
#include "org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls.h"
#include <fstream>
#include "string_match_kernels.h"
#include <streambuf>

using namespace parallelus;

struct NativeData {
    std::shared_ptr<Runtime> runtime;
    std::shared_ptr<Program> program;   
};

JNIEXPORT jlong JNICALL Java_org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls_nativeInit
        (JNIEnv *env, jobject self) {

    JavaVM *jvm;
    env->GetJavaVM(&jvm);
    if(!jvm) return (jlong) nullptr;

    auto dataPointer = new NativeData();
    dataPointer->runtime = std::make_shared<Runtime>(jvm,false);
    dataPointer->program = std::make_shared<Program>(dataPointer->runtime, StringMatchKernels);

    return (jlong) dataPointer;
}

JNIEXPORT void JNICALL Java_org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls_nativeCleanUp
        (JNIEnv *env, jobject self, jlong dataLong) {
    auto dataPointer = (NativeData *) dataLong;
    delete dataPointer;
}
JNIEXPORT void JNICALL Java_org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls_nativeTweetStringMatch
        (JNIEnv *env, jobject self, jlong dataLong, jstring target, jstring tweet, jintArray sizes, jintArray output) {
    
    char *tweet_str = (char*) env->GetStringUTFChars(tweet, 0);
    char *target_str = (char*) env->GetStringUTFChars(target, 0);
    auto output_ref = (jintArray) env->NewGlobalRef(output);

    int tweet_str_size = strlen(tweet_str);
    int target_str_size = strlen(target_str);
    int num_tokens = ((int) env->GetArrayLength(sizes)); 

    int* tweet_info = (int*) malloc (sizeof(int)*num_tokens*2);

    jint *infos = env->GetIntArrayElements(sizes, 0);
    for(int i=0; i < num_tokens*2; i+=2){
    	if(i==0){
    		tweet_info[i] = 0;
    		tweet_info[i+1] = infos[0];
    	}
    	else{
    		tweet_info[i] = tweet_info[i-2] + tweet_info[i-1] + 1;
    		tweet_info[i+1] = infos[i/2];
    	}
    	//printf("[%d][%d]", tweet_info[i],tweet_info[i+1]);
    }
    env->ReleaseIntArrayElements(sizes, infos, 0);
    //printf("\n");

    for(int i=0; i < num_tokens*2; i+=2){
    	int tbegin = tweet_info[i];
    	int tsize = tweet_info[i+1];

    	char* token = (char*) malloc (sizeof(char)*tsize);

		memcpy(token, &tweet_str[tbegin], tsize);
		token[tsize+1] = '\0';
    	//printf("[%d]%s=%d\n",i/2,token, tsize);
    }

    /*================================ BUFFER DECLARATION ============================================*/
    auto dataPointer = (NativeData *) dataLong;
    auto i_tweet_buffer = std::make_shared<Buffer>(Buffer::sizeGenerator(tweet_str_size, Buffer::CHAR));
    auto i_target_buffer = std::make_shared<Buffer>(Buffer::sizeGenerator(target_str_size, Buffer::CHAR));
    auto i_infos_buffer = std::make_shared<Buffer>(Buffer::sizeGenerator(num_tokens*2, Buffer::INT));
    auto o_map_buffer = std::make_shared<Buffer>(Buffer::sizeGenerator(num_tokens, Buffer::INT));
    auto output_buffer = std::make_shared<Buffer>(Buffer::sizeGenerator(2, Buffer::INT));

    i_target_buffer->setSource(target_str);
    i_tweet_buffer->setSource(tweet_str);
    i_infos_buffer->setSource(tweet_info);

    auto task = std::make_unique<Task>(dataPointer->program);
    task->addKernel("convtl");
    task->addKernel("match_string_map");
    task->addKernel("reduce_min");

    task->setConfigFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type) {

    	kernelHash["convtl"]
    		->setArg(0, tweet_str_size, type)
    		->setArg(1, i_tweet_buffer, type)
    		->setWorkSize(tweet_str_size,1,1,type);

	    kernelHash["match_string_map"]
			->setArg(0, i_target_buffer, type)
			->setArg(1, target_str_size, type)
			->setArg(2, i_tweet_buffer, type)
			->setArg(3, i_infos_buffer, type)
			->setArg(4, o_map_buffer, type)
			->setWorkSize(num_tokens,1,1,type);

		kernelHash["reduce_min"]
    		->setArg(0, o_map_buffer, type)
    		->setArg(1, num_tokens, type)
    		->setArg(2, output_buffer,type)
    		->setWorkSize(1,1,1,type);
    });

    task->setFinishFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type){   
        //device->JNIEnv()->MonitorEnter(lock1);
		output_buffer->copyToJArray(device->JNIEnv(),output_ref,1);
    	//device->JNIEnv()->MonitorExit(lock1);
    });

    dataPointer->runtime->submitTask(std::move(task),2);
}


JNIEXPORT void JNICALL Java_org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls_nativeWaitFinish
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->finish();
}

JNIEXPORT void JNICALL Java_org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls_nativeKill
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->kill();
}
