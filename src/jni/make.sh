#!/bin/sh
jav_src=".."
pus_src="./ParallelUS/runtime"
app_src="."
app_file="org_parallelus_samples_twitterstream_TweetStringMatchNativeCalls" #MUDAR
app_name="TweetStringMatch" #MUDAR
runtime_name="ParallelUSRuntime"

pus_objs=$pus_src/objs
pus_lib=$pus_src/lib

app_objs=$app_src/objs
app_lib=$app_src/lib

mkdir -p $pus_objs
mkdir -p $pus_objs/dynloader
mkdir -p $pus_lib
mkdir -p $app_objs
mkdir -p $app_lib

rm -f $app_objs/*.o
rm -f $app_objs/*.o.d
rm -f $app_lib/*.so
rm -f $pus_objs/*.o
rm -f $pus_objs/*.o.d
rm -f $pus_objs/dynloader/*.o
rm -f $pus_objs/dynloader/*.o.d
rm -f $pus_lib/*.so

CCP="clang++"
CC="clang"
INCLUDE="-I$pus_src/include -I/usr/local/include -I$JAVA_HOME/include -I$JAVA_HOME/include/linux -I$pus_src"
FLAGS_I="-MMD -MP -MF"
FLAGS="-fPIC -DLLVM_ENABLE_LLD=ON -Ofast -Wall -Wextra -std=c++11 -pthread -Ofast -Wall -Wextra -Wunused-function -Wunused-variable -Wno-unused-parameter -Werror=format-security -Wno-deprecated-register -Werror -std=c++14 -fexceptions -stdlib=libc++"
DYN_FLAGS="-fPIC -Ofast -Wall -Wextra -Ofast -Wall -Wextra -Wno-deprecated-register"
FLAG_SHARED="-fPIC -no-canonical-prefixes -Wl,--build-id -Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now -Wl,--warn-shared-textrel -Wl,--fatal-warnings  -lc -lm"
FLAG_TMP="-fPIC -Wl,--no-undefined -Wl,--no-as-needed -ldl -stdlib=libc++ -pthread"
INCLUDE_SHARED="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
PUS_OBJS=""

echo "[Compile]\t$app_name <= $app_file.cpp";
$CCP $FLAGS_I $app_objs/$app_file.o.d  $INCLUDE -I$app_src $FLAGS -c  $app_src/$app_file.cpp -o $app_objs/$app_file.o

for i in "Buffer" "Device" "Kernel" "Program" "Runtime" "Task" "NetworkManager" "SchedulerFCFS" "SchedulerHEFT" "SchedulerPAMS"
do
   echo "[Compile]\t$runtime_name <= $i.cpp"
   $CCP $FLAGS_I $pus_objs/$i.o.d  $INCLUDE $FLAGS -c $pus_src/src/parallelus/$i.cpp -o $pus_objs/$i.o
   PUS_OBJS="$PUS_OBJS $pus_objs/$i.o"
done

echo "[Compile]\t$runtime_name <= dynLoader.c"
$CC $FLAGS_I $pus_objs/dynloader/dynLoader.o.d $INCLUDE $DYN_FLAGS -c $pus_src/src/parallelus/dynloader/dynLoader.c -o $pus_objs/dynloader/dynLoader.o

echo "[SharedLibrary]\tlib$runtime_name.so"
PUS_OBJS="$PUS_OBJS $pus_objs/dynloader/dynLoader.o"
$CCP -Wl,-soname,lib$runtime_name.so -shared $PUS_OBJS $INCLUDE_SHARED $FLAG_TMP -o $pus_lib/lib$runtime_name.so

echo "[SharedLibrary]\tlib$app_name.so"
$CCP -Wl,-soname,lib$app_name.so -shared $app_objs/$app_file.o $pus_lib/lib$runtime_name.so $INCLUDE_SHARED $FLAG_TMP -o $app_lib/lib$app_name.so

mkdir -p $jav_src/libs
echo "[INSTALL]\tlib$runtime_name.so => $jav_src/libs/"
rm -f $jav_src/libs/lib$runtime_name.so
cp $pus_lib/lib$runtime_name.so $jav_src/libs/

echo "[INSTALL]\tlib$app_name.so => $jav_src/libs/"
rm -f $jav_src/libs/lib$app_name.so
cp $app_lib/lib$app_name.so $jav_src/libs/