/**                                               _    __ ____
 *   _ __  ___ _____   ___   __  __   ___ __     / |  / /  __/
 *  |  _ \/ _ |  _  | / _ | / / / /  / __/ /    /  | / / /__
 *  |  __/ __ |  ___|/ __ |/ /_/ /__/ __/ /__  / / v  / /__
 *  |_| /_/ |_|_|\_\/_/ |_/____/___/___/____/ /_/  /_/____/
 *
 */

#ifndef KERNELS_H
#define KERNELS_H

/// OpenCL kernels are defined in this file.
const char StringMatchKernels[] =
    "__kernel void match_string_map(__global char* keyword, int keywordSize, __global char* tweet, __global int* infos, __global int* output){\n"
	"	__local int matrix[250];														\n"
	"	int gid = get_global_id(0);														\n"
	"	int currentWordSize = infos[(gid*2)+1];											\n"
	"	int currentWordBegin = infos[(gid*2)];											\n"
	"	int col = currentWordSize+1;													\n"
	"   //char A  = (char)('A');                                                      	\n"
  	"   //char Z  = (char)('Z');                                                      	\n"
  	"   //char aA = (char)('a' - 'A');                                                	\n"
	"	for (int i = 0; i < keywordSize+1; i++){										\n"
	"		matrix[i*col + 0] = i;														\n"
	"	}																				\n"
	"	for (int i = 1; i < currentWordSize+1; i++){									\n"
	"		matrix[0*col + i] = i;														\n"
	"	}																				\n"
	"	for (int i = 1; i < keywordSize+1; i++){										\n"
	"		for (int j = 1; j < currentWordSize+1; j++){								\n"
	"			int e1 = matrix[(i-1)*col + j] + 1;										\n"
	"			int e2 = matrix[i*col + (j-1)] + 1;										\n"
	"			char kc = keyword[(i-1)];												\n"
	"			char cc = tweet[(j-1)+currentWordBegin];								\n"
	"			//char lkc = (char)(kc + aA);											\n"
	"			//char lcc = (char)(cc + aA);											\n"
	"			//char keyChar = ((kc >= A) && (kc <= Z)) ? lkc : kc;					\n"
	"			//char curChar = ((cc >= A) && (cc <= Z)) ? lkc : cc;					\n"
	"			//int e3 = ((keyChar == curChar)?0:1); 									\n"
	"			int e3 = ((kc == cc)?0:1);												\n"
	"			int e4 = e3 + matrix[(i-1)*col + (j-1)];								\n"
	"			matrix[i*col + j] = min(min(e1, e2), e4);								\n"
	"		}																			\n"
	"	}																				\n"
	"	output[gid] = matrix[keywordSize*col + currentWordSize];						\n"	
	"}																					\n"
	"__kernel void convtu(int n, __global char *c){						  				\n"
  	"   int i = get_global_id(0);                                                       \n"
  	"   char a  = (char)('a');                                                          \n"
  	"   char z  = (char)('z');                                                          \n"
  	"   char aA = (char)('a' - 'A');                                                    \n"
  	"   if(i < n) {                                                                     \n"
  	"       char uc = (char)(c[i] - aA);                                                \n"
  	"       c[i] = ((c[i] >= a) && (c[i] <= z)) ? uc : c[i];                            \n"
  	"	}                                                                               \n"
  	"}																					\n"
  	"__kernel void convtl(int n, __global char *c){						  				\n"
  	"   int i = get_global_id(0);                                                       \n"
  	"   char A  = (char)('A');                                                          \n"
  	"   char Z  = (char)('Z');                                                          \n"
  	"   char aA = (char)('a' - 'A');                                                    \n"
  	"   if(i < n) {                                                                     \n"
  	"       char uc = (char)(c[i] + aA);                                                \n"
  	"       c[i] = ((c[i] >= A) && (c[i] <= Z)) ? uc : c[i];                            \n"
  	"	}                                                                               \n"
  	"}																					\n"
  	"__kernel void reduce_min(__global int *c, int n,__global int *out){				\n"
  	"	int min = 99;																	\n"
  	"	for(int i=0; i<n; i++){															\n"
  	"		if(min > c[i]){																\n"
	" 				min = c[i];															\n"
  	"		}																			\n"
	"	}                                                       						\n"
  	"   out[0] = min;                                                          			\n"
  	"}																					\n";
 #endif