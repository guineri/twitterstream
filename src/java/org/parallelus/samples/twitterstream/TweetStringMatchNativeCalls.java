package org.parallelus.samples.twitterstream;

import java.util.ArrayList;
import java.util.List;

public class TweetStringMatchNativeCalls implements TweetStringMatchOperator {
    private long dataPointer;

    private native long nativeInit();
    private native void nativeCleanUp(long dataPointer);
    private native void nativeTweetStringMatch(long dataPointer, String target, String tweet, int[] sizes, int[] output);
    private native void nativeWaitFinish(long dataPointer);
    private native void nativeKill(long dataPointer);

    @Override

    public void tweetStringMatch(String target, String tweet, int[] sizes, int[] output) {
        nativeTweetStringMatch(dataPointer,target,tweet,sizes,output);
    }


    public void waitFinish(){
        nativeWaitFinish(dataPointer);
    }

    public void killRuntime(){
        nativeKill(dataPointer);
    }

     public boolean inited() {
        return dataPointer != 0;
    }

    TweetStringMatchNativeCalls() {
        dataPointer = nativeInit();
    }

    protected void finalize() throws Throwable {
        try {
            if(dataPointer != 0)
                nativeCleanUp(dataPointer);
        } catch(Throwable t) {
            throw t;
        } finally {
            super.finalize();
        }
    }

    static {
        System.loadLibrary("TweetStringMatch");
    }
}