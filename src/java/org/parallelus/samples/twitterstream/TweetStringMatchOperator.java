package org.parallelus.samples.twitterstream;

import java.util.ArrayList;
import java.util.List;

public interface TweetStringMatchOperator {
    void tweetStringMatch(String target, String tweet, int[] sizes, int[] output);
    void waitFinish();
    void killRuntime();
}
