package org.parallelus.samples.twitterstream;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;

public class StreamProcessing {

	public static void printResult(String tweet, int[] result){
		if(result[0] < 2){
			System.out.println(tweet + "\n======================\n");
		}
	}

	public static void main(String[] args) {
      
      	String keyword = "trump";
		int keywordSize = keyword.length();
		Vector<String> tweetsBuffer = new Vector<>();
		Vector<int[]>  sizesBuffer = new Vector<int[]>();
		Vector<int[]>  outputBuffer = new Vector<int[]>();
		
		try {
			//BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			File file = new File("example.txt");
			FileInputStream fis = null;
			fis = new FileInputStream(file);
			BufferedReader input = new BufferedReader(new InputStreamReader(fis));

      		TweetStringMatchNativeCalls tweetStringMatchNative;
      		tweetStringMatchNative = new TweetStringMatchNativeCalls();

      		/*================== Buffered way ===========================================*/
	        int bufferSize = 100;
	        for(int i=0; i<bufferSize; i++){
	        	int[] out_tmp = new int[2];
	        	out_tmp[0] = out_tmp[1] = -2;
	        	outputBuffer.add(out_tmp);
	        }

			while (true) {	

				boolean submited = false;
			
				String currentTweet = input.readLine();
				if(currentTweet == null) break;
				String[] book = currentTweet.split(" ");
				int[] currentStringSizes = new int[book.length];
				for(int i=0; i<book.length; i++) currentStringSizes[i] = book[i].length();
				int[] currentOutput = new int[2];
				currentOutput[0] = currentOutput[1] = -1;
				//System.out.println(count);
				if (!(book.length == 1 && book[0].equals(""))) {
					while(!submited){
						for(int i=0; i<bufferSize; i++){
							if(outputBuffer.get(i)[0] == -2){
								tweetsBuffer.add(i,currentTweet);

								sizesBuffer.add(i,currentStringSizes);
								
								outputBuffer.removeElementAt(i);
								outputBuffer.add(i,currentOutput);

								tweetStringMatchNative.tweetStringMatch(keyword,tweetsBuffer.get(i),sizesBuffer.get(i),outputBuffer.get(i));	
								submited = true;
								break;
							}
							else if(outputBuffer.get(i)[0] > -1){
								printResult(tweetsBuffer.get(i), outputBuffer.get(i));

								tweetsBuffer.removeElementAt(i);
								tweetsBuffer.add(i,currentTweet);
								
								sizesBuffer.removeElementAt(i);
								sizesBuffer.add(i,currentStringSizes);
								
								outputBuffer.removeElementAt(i);
								outputBuffer.add(i,currentOutput);

								tweetStringMatchNative.tweetStringMatch(keyword,tweetsBuffer.get(i),sizesBuffer.get(i),outputBuffer.get(i));
								submited = true;
								break;
							}
						}
					}
				}
			}

			tweetStringMatchNative.waitFinish();
			for(int i=0; i<bufferSize; i++){
				if(outputBuffer.get(i)[0] > -1){
					printResult(tweetsBuffer.get(i), outputBuffer.get(i));
				}
			}
      		/*============================== Sequential way =========================================*/
      		/*int count = 0;
      		int[] output = new int[2];
			while (true) {		

				String currentTweet = input.readLine();
				if(currentTweet == null) break;
				String[] book = currentTweet.split(" ");
				count++;
				if (!(book.length == 1 && book[0].equals(""))) {
					int[] stringSizes = new int[book.length];
					for(int i=0; i<book.length; i++) stringSizes[i] = book[i].length();
					output[0] = -1;

					tweetStringMatchNative.tweetStringMatch(keyword,currentTweet,stringSizes,output);
					tweetStringMatchNative.waitFinish();

					int result = output[0];

					if(result < 2){
						System.out.println(currentTweet + "\n======================\n");
					}
				}
				System.out.println(count);
			}
      		
      		tweetStringMatchNative.waitFinish();*/
			tweetStringMatchNative.killRuntime();

		} catch (IOException e) {
			System.out.println(e);
		}
		//System.exit(0);
	}
}